package main;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		int number = 0;
		while (number < 15) {
			System.out.println(number);
			number++;
		}
		
		
		String text1 = "Hello there my friend! How are you today?";
		for (int i = 0; i < text1.length(); i++) {
			System.out.println(text1.charAt(i));
		}
		
		nestedLopps(args);
		whileLoopExample(args);
	}
	
	public static void nestedLopps(String[] args) {
	
		for (int row = 0; row < 5 ; row++) {
			System.out.print("Row: " + row + ": ");
			for (int column = 0; column < 10; column++) {
				System.out.print(column + " ");
			}
			System.out.println();
		}
		
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
	
	public static void whileLoopExample(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Hello");
		String answer = sc.nextLine();
		while(answer.equals("Hello")) {
			System.out.println("Hello");
			answer =sc.nextLine();
		}
		System.out.println("Goodbye!");
	}
}
